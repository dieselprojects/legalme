/**
 * Created by idanhahn on 10/4/2016.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({

    testator: {type: Testator, require: true},
    will_gender: {type: String, require: true},
    date_en: {type: Date, require: true, default: Date.now},
    main_sections: {type: [Section]}
    
});

module.exports = mongoose.model('Will', schema);
