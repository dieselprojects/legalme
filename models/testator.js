/**
 * Created by idanhahn on 10/16/2016.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Address = require('./address');

var schema = new Schema({
    block_type: {type: String, require: true},
    first_name: {type: String,require: true},
    last_name: {type: String,require: true},
    id: {type: String,require: true},
    gender: {type: String,require: true},
    address: {type: Address, require: true}
});

module.exports = mongoose.model('Testator', schema);
