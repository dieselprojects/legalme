import { LegalMePage } from './app.po';

describe('legal-me App', function() {
  let page: LegalMePage;

  beforeEach(() => {
    page = new LegalMePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
