var express = require('express');
var router = express.Router();

var http = require('http');


router.post('/generate_will', function (req, res, next) {


    // assume req.will_recipe contains json to create will
    // send http request to local host 3333 with json and wait for a response
    // response contain same json with additional blob of will pdf
    // TODO: create new schema model from returned and store
    
    
    // generate Java server request
    var body = JSON.stringify(req.body);
    var options = {
        hostname: '',
        port: 3333,
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        }
    };
    var serverReq = http.request(options, function(res1){
                res1.setEncoding('utf8');
        data = "";        
        res1.on('data', function(chunk){
                    
                    console.log("response");
                    console.log(chunk)
                    data += chunk;
                    console.log("Done chunk");
                    //console.log(jsonResponse);
                    //console.log(res);
                });
                res1.on('end', function(){
                    console.log('No more data in response.');
                    var jsonResponse = JSON.parse(data);
                    console.dir(jsonResponse);

                    res.status(201).json({
                        message: 'Saved message',
                        obj: jsonResponse
                    });
                });
    });

    serverReq.on('error', function(e){
        console.log(e)
    });

    serverReq.write(body);
    serverReq.end();


    //var willRecipe = new Will({
    //    content: req.body.content
    //});
    /* 
     message.save(function (err, result) {
     if (err) {
     return res.status(404).json({
     title: 'An error occurred',
     error: err
     });
     }
     res.status(201).json({
     message: 'Saved message',
     obj: result
     });
     });
     */
});
//var Message = require('../models/message');

module.exports = router;