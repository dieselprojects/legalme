import {Injectable} from "@angular/core";
import {Section} from "./models/section";

/**
 * Created by idanhahn on 10/4/2016.
 */

@Injectable()
export class WillService {

    // key is section type
    private sections:{ [key:string]:Section } = {};


    constructor() {
    }


    // --------------------- //
    // -- Handle Testator -- //
    // --------------------- //
/*
    addWill0Form(will0FormObj:Will0FormObject) {
        console.log(will0FormObj);
        this.user.firstName = will0FormObj.userFirstName;
    }
*/
    // --------------------- //
    // -- Handle sections -- //
    // --------------------- //

    addSection(key:string, section:Section) {

        this.sections[key] = section;
        console.log("WillService: Add Section " + key);
        console.log("WillService: Add Section(Array status) ", this.sections);

    }

    getSection(key:string) {

        console.log("WillService: Get Section " + key);
        console.log("WillService: Get Section(result) ", this.sections[key]);
        console.log("WillService: Get Section(Array status) ", this.sections);

        if (key in this.sections) {
            return this.sections[key];
        } else {
            console.error("WillService: Get Section Failed with key " + key);
            console.error(this.sections);
            return null;
        }

    }

    deleteSection(key:string) {
        console.error("Implement WillService delete");
    }

    updateSection(key:string, section:Section) {

        // check if exists

        if (key in this.sections) {
            this.sections[key] = section;
        } else {
            console.error("WillService: Update Section Failed with key " + key);
            console.error(this.sections);
            return;
        }

        console.log("WillService: Update Section " + key);
        console.log("WillService: Update Section(result) ", this.sections[key]);
        console.log("WillService: Update Section(Array status) ", this.sections);

    }

    // Debug helpers:
    printSections() {
        console.log(this.sections);
    }

}
