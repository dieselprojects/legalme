/**
 * Created by idanhahn on 10/24/2016.
 */
import {Component} from "@angular/core";
import {FormGroup, FormControl, FormArray, Validators} from "@angular/forms";

@Component({
  selector: 'app-decree-create',
  templateUrl: 'decree-create.component.html',
  styleUrls: ['decree-create.component.css']
})
export class DecreeCreateComponent {

  form:FormGroup;

  asset_type_options = [
    'מקרקעין',
    'מטלטלין',
    'כספים',
    'אחר'
  ]

  asset_type:string;
  asset_type_heb:string;

  constructor() {
    
    let addressFG = new FormGroup({
      'street': new FormControl('', Validators.required),
      'house': new FormControl('', Validators.required),
      'entry': new FormControl(''),
      'apartment': new FormControl(''),
      'city': new FormControl('', Validators.required),
      'isOther': new FormControl(''),
      'description': new FormControl('')
    });
    
    let legalAddressFG = new FormGroup({
      'group': new FormControl(''),
      'section': new FormControl(''),
      'sub_section': new FormControl('')
    })

    let assetDescriptionFG = new FormGroup({
      're_house_address': addressFG,
      're_house_legal_address': legalAddressFG,
      'ch_car_model': new FormControl(''),
      'ch_car_number': new FormControl(''),
      'ch_car_year': new FormControl(''),
      'mo_number': new FormControl(''),
      'mo_bank': new FormControl(''),
      'mo_branch': new FormControl(''),
      'ot_other': new FormControl('')
    })
    
    let beneficiaryFG = new FormGroup({
      'relation': new FormControl(''),
      'first_name': new FormControl(''),
      'last_name': new FormControl(''),
      'gender': new FormControl(''),
      'id': new FormControl(''),
      'birth_date': new FormControl(''),
      'address': addressFG,
      'ownership': new FormControl(''),
      'personal_comment': new FormControl(''),
      'is_main': new FormControl('')
    })

    let assetFG = new FormGroup({
      'asset_type': new FormControl(''),
      'asset_sub_type': new FormControl(''),
      'asset_description': assetDescriptionFG
    })
    
    this.form = new FormGroup({
      'asset': assetFG,
      'beneficiary_main': beneficiaryFG,
      'beneficiaries': new FormArray([beneficiaryFG])
    })
  }


  onSelect(option) {
    this.asset_type_heb = this.asset_type_options[option];
    switch (option) {
      case "0":
        this.asset_type = "re";
        break;
      case "1":
        this.asset_type = "ch";
        break;
      case "2":
        this.asset_type = "mo";
        break;
      case "3":
        this.asset_type = "ot";
        break;
    }
  }

}
