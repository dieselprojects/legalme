/**
 * Created by idanhahn on 10/4/2016.
 */
import { Component } from "@angular/core";

@Component({
  selector: 'app-beneficiaries-list',
  templateUrl: 'beneficiaries-list.component.html',
  styleUrls:['beneficiaries-list.component.css']
})
export class BeneficiariesListComponent {
}
