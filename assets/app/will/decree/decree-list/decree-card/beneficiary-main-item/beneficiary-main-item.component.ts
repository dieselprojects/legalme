/**
 * Created by idanhahn on 10/4/2016.
 */
import { Component } from "@angular/core";

@Component({
  selector: 'app-beneficiary-main-item',
  templateUrl: 'beneficiary-main-item.component.html',
  styleUrls:['beneficiary-main-item.component.css']
})
export class BeneficiaryMainItemComponent {
}
