/**
 * Created by idanhahn on 10/4/2016.
 */
import { Component } from "@angular/core";

@Component({
    selector: 'app-asset-description',
    templateUrl: 'asset-description.component.html',
    styleUrls:['asset-description.component.css']
})
export class AssetDescriptionComponent {
}
