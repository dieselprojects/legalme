import {AssetDescription} from "../asset-description/asset-description";
import {Beneficiary} from "../beneficiary/beneficiary";
import {FormGroup} from "@angular/forms";
/**
 * Created by idanhahn on 10/24/2016.
 */

export class Decree {

  block_type:string;
  type:string;
  sub_type:string;
  asset:Asset;
  beneficiary_main:Beneficiary;
  beneficiaries:Beneficiary[];


  constructor(form?:FormGroup) {
    switch (form.value.type) {
      case "re":
        break;
      case "ch":
        break;
      case "mo":
        break;
      case "ot":
        break;
    }
  }
}
