/**
 * Created by idanhahn on 10/4/2016.
 */
import { Component } from "@angular/core";
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {TestatorService} from "../testator-service";
import {Router} from "@angular/router";
import {Testator} from "../testator";

@Component({
    selector: 'app-testator-create',
    templateUrl: 'testator-create.component.html',
    styleUrls:['testator-create.component.css']
})
export class TestatorCreateComponent {

  // form options:
  genders =[
    'זכר',
    'נקבה'
  ];

  form: FormGroup;

  constructor(
    private router: Router,
    private testatorService: TestatorService
  ){

    this.form = new FormGroup({
      'first_name' : new FormControl(''),
      'last_name' : new FormControl(''),
      'gender' : new FormControl(''),
      'id' : new FormControl(''),
      'birth_date': new FormControl(''),
      'address': new FormGroup({
        'street': new FormControl('', Validators.required),
        'house': new FormControl('', Validators.required),
        'entry': new FormControl(''),
        'apartment': new FormControl(''),
        'city': new FormControl('', Validators.required),
        'isOther': new FormControl(''),
        'description': new FormControl('')
      })
    });
  }


  onSubmit(){

    this.testatorService.setTestator(new Testator(this.form));
    this.router.navigate(['will','decree']);

  }

}
