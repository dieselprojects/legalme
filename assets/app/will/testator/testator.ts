import {Address} from "../address/address";
import {FormGroup} from "@angular/forms";
/**
 * Created by idanhahn on 10/24/2016.
 */

export class Testator{

  block_type: string;
  first_name: string;
  last_name: string;
  gender: string;
  id: string;
  birth_date: Date;
  address: Address;

  constructor(form?: FormGroup){

    this.block_type = "testator";
    this.first_name = form.value.first_name;
    this.last_name = form.value.last_name;
    this.gender = form.value.gender;
    this.id = form.value.id;
    this.birth_date = form.value.birth_date;

    this.address = new Address(<FormGroup>form.controls['address']);

  }
}
