import {Injectable} from "@angular/core";
import {Testator} from "./testator";
/**
 * Created by idanhahn on 10/24/2016.
 */
  
@Injectable()
export class TestatorService {
 
  testator: Testator;
  
 
  setTestator(testator: Testator){
    this.testator = testator;
    console.log("Setting testator ", this.testator);
  }
  
  getTestator(): Testator{
    return this.testator;
  }
  
}
