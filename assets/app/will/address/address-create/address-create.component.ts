/**
 * Created by idanhahn on 10/4/2016.
 */
import {Component, Input} from "@angular/core";
import {FormGroup, FormControl} from "@angular/forms";

@Component({
    selector: 'app-address-create',
    templateUrl: 'address-create.component.html',
    styleUrls:['address-create.component.css']
})
export class AddressCreateComponent {

  @Input('group') form: FormGroup;
  
  constructor(){}
  

}
