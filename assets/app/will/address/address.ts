import {FormGroup} from "@angular/forms";
/**
 * Created by idanhahn on 10/4/2016.
 */
export class Address {

  block_type:string;
  street:string;
  house:string;
  entry:string;
  apartment:string;
  city:string;
  isOther: boolean;
  description:string;

  constructor(form?:FormGroup) {

    if (form.value.isOther){
      this.block_type = "address_other"
      this.description = form.value.other;
    }

    if (form.value.entry == "" && form.value.apartment == ""){
      this.block_type = "address_3";
    } else if (form.value.apartment == ""){
      this.block_type = "address_2";
      this.entry = form.value.entry;
    } else if (form.value.entry == ""){
      this.block_type = "address_1";
      this.apartment = form.value.apartment;
    } else {
      this.block_type = "address_0";
    }
    this.street = form.value.street;
    this.city = form.value.city;
    this.house = form.value.house;
  }


}
