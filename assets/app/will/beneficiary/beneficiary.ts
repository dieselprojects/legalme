import {Address} from "../address/address";
/**
 * Created by idanhahn on 10/21/2016.
 */

export class Beneficiary{

    block_type: string;
    relation: string;
    first_name: string;
    last_name: string;
    id: string;
    birth_date: Date;
    address: Address;
    ownership: number;
    personal_comment: string;
    is_main: boolean;

    
    getFullName(): string{
        return this.first_name + " " + this.last_name;        
    }
    
}
