import {FormGroup} from "@angular/forms";
import {AssetDescription} from "./asset-description/asset-description";
/**
 * Created by idanhahn on 10/20/2016.
 */

export class Asset {

  block_type:string;
  asset_type:string;
  asset_type_heb: string;
  asset_sub_type: string;
  asset_sub_type_heb: string;
  asset_description: AssetDescription;

  constructor(form?:FormGroup) {

    switch (form.value.asset_type) {
      case "re":
        this.create_re(form);
        break;
      case "ch":
        this.create_ch(form);
        break;
      case "mo":
        this.create_mo(form);
        break;
      case "ot":
        this.create_ot(form);
        break;
    }


  }

  create_re(form:FormGroup) {
  }

  create_ch(form:FormGroup) {

  }

  create_mo(form:FormGroup) {

  }

  create_ot(form:FormGroup) {

  }
}
