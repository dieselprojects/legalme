/**
 * Created by idanhahn on 10/24/2016.
 */
import {Component, Input} from "@angular/core";
import {FormGroup} from "@angular/forms";

@Component({
  selector: 'app-asset-create',
  templateUrl: 'asset-create.component.html',
  styleUrls:['asset-create.component.css']
})
export class AssetCreateComponent {
  
  @Input('group') public form: FormGroup;
  @Input() asset_type: string;

  

  constructor(){}
}
