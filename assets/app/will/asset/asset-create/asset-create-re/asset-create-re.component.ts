/**
 * Created by idanhahn on 10/24/2016.
 */
import {Component, Input} from "@angular/core";
import {FormGroup} from "@angular/forms";

@Component({
  selector: 'app-asset-create-re',
  templateUrl: 'asset-create-re.component.html',
  styleUrls:['asset-create-re.component.css']
})
export class AssetCreateReComponent {

  @Input('group') public form: FormGroup;


  asset_sub_type_options = [
    'דירה',
    'בית פרטי',
    'בית',
    'חנות',
    'משרד',
    'שטח',
    'שטח חקלאי',
    'חניה',
    'אחר'
  ];

  asset_sub_type:string;
  asset_sub_type_heb:string;

  onSelect(option) {
    this.asset_sub_type_heb = this.asset_sub_type_options[option];
    switch (option) {
      case "0":
        this.asset_sub_type = "re0";
        break;
      case "1":
        this.asset_sub_type = "re1";
        break;
      case "2":
        this.asset_sub_type = "re2";
        break;
      case "3":
        this.asset_sub_type = "re3";
        break;
      case "4":
        this.asset_sub_type = "re4";
        break;
      case "5":
        this.asset_sub_type = "re5";
        break;
      case "6":
        this.asset_sub_type = "re6";
        break;
      case "7":
        this.asset_sub_type = "re7";
        break;
      case "8":
        this.asset_sub_type = "re8";
        break;
    }
    console.log(this.asset_sub_type)
  }
  
}
