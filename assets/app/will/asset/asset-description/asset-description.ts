import {Address} from "../../address/address";
import {LegalAddress} from "./asset-description-re/legal-address/legal-address";
/**
 * Created by idanhahn on 10/24/2016.
 */

export class AssetDescription{
  block_type: string;

  description_type: string;
  
  // for realestate
  re_house_address:Address;
  re_house_legal_address:LegalAddress;

  // for chattels
  ch_car_model: string;
  ch_car_number: string;
  ch_car_year: string;

  // for money
  mo_number: string;
  mo_bank: string;
  mo_branch: string;

  // general:
  ot_other: string;
}
