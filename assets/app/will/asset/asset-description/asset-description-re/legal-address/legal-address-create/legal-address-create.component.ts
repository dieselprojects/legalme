/**
 * Created by idanhahn on 10/4/2016.
 */
import {Component, Input} from "@angular/core";
import {FormGroup, FormControl} from "@angular/forms";

@Component({
    selector: 'app-legal-address-create',
    templateUrl: 'legal-address-create.component.html',
    styleUrls:['legal-address-create.component.css']
})
export class LegalAddressCreateComponent {

  @Input('group') form: FormGroup;
  
  constructor(){}
  

}
