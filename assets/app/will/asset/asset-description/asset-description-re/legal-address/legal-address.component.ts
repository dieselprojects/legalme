/**
 * Created by idanhahn on 10/4/2016.
 */
import {Component, Input} from "@angular/core";
import {FormGroup} from "@angular/forms";

@Component({
    selector: 'app-legal-address',
    templateUrl: 'legal-address.component.html',
    styleUrls:['legal-address.component.css']
})
export class LegalAddressComponent {
 
  @Input() show: boolean;
  @Input('group') public form: FormGroup;
  @Input() title;
  
  constructor(){
  }
  
  
}
