import {Routes} from "@angular/router";
import {TestatorComponent} from "./testator/testator.component";
import {DecreeComponent} from "./decree/decree.component";
/**
 * Created by idanhahn on 10/4/2016.
 */

export const WILL_ROUTES : Routes = [
    { path: '', component: TestatorComponent },
    { path: 'testator', component: TestatorComponent },
    { path: 'decree', component: DecreeComponent }
];
