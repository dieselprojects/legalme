import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {routing} from "./app.routing";
import {HttpModule} from "@angular/http";
import {ReactiveFormsModule, FormsModule} from "@angular/forms";

import {AppComponent} from "./app.component";
import {HeaderComponent} from "./header/header.component";
import {HomeComponent} from "./home/home.component";
import {WillComponent} from "./will/will.component";
import {TestatorCreateComponent} from "./will/testator/testator-create/testator-create.component";
import {DecreeComponent} from "./will/decree/decree.component";
import {DecreeCreateComponent} from "./will/decree/decree-create/decree-create.component";
import {BeneficiariesListComponent} from "./will/decree/decree-list/decree-card/beneficiaries-list/beneficiaries-list.component";
import {DecreeListComponent} from "./will/decree/decree-list/decree-list.component";
import {DecreeCardComponent} from "./will/decree/decree-list/decree-card/decree-card.component";
import {AssetDescriptionComponent} from "./will/decree/decree-list/decree-card/asset-description/asset-description.component";
import {BeneficiaryMainItemComponent} from "./will/decree/decree-list/decree-card/beneficiary-main-item/beneficiary-main-item.component";
import {BeneficiaryItemComponent} from "./will/decree/decree-list/decree-card/beneficiaries-list/beneficiary-item/beneficiary-item.component";
import {TestatorComponent} from "./will/testator/testator.component";
import {WillService} from "./will/will.service";
import {TestatorService} from "./will/testator/testator-service";
import {DecreeService} from "./will/decree/decree-service";
import {AddressComponent} from "./will/address/address.component";
import {AddressCreateComponent} from "./will/address/address-create/address-create.component";
import {AssetCreateComponent} from "./will/asset/asset-create/asset-create.component";
import {AssetCreateChComponent} from "./will/asset/asset-create/asset-create-ch/asset-create-ch.component";
import {BeneficiariesCreateComponent} from "./will/beneficiary/beneficiaries-create/beneficiaries-create.component";
import {BeneficiaryMainCreateComponent} from "./will/beneficiary/beneficiary-main-create/beneficiary-main-create.component";
import {AssetCreateReComponent} from "./will/asset/asset-create/asset-create-re/asset-create-re.component";
import {AssetCreateMoComponent} from "./will/asset/asset-create/asset-create-mo/asset-create-mo.component";
import {AssetCreateOtComponent} from "./will/asset/asset-create/asset-create-ot/asset-create-ot.component";
import {LegalAddressComponent} from "./will/asset/asset-description/asset-description-re/legal-address/legal-address.component";
import {LegalAddressCreateComponent} from "./will/asset/asset-description/asset-description-re/legal-address/legal-address-create/legal-address-create.component";

@NgModule({
  declarations: [
    AppComponent,

    // General components
    HeaderComponent,
    HomeComponent,
  
    AddressComponent,
      AddressCreateComponent,
   
    LegalAddressComponent,
      LegalAddressCreateComponent,
    
    // Will:
    WillComponent,
      // Testator:
      TestatorComponent,
        TestatorCreateComponent,
      // Decree:
      DecreeComponent,
        DecreeCreateComponent,
          AssetCreateComponent,
            AssetCreateMoComponent,
            AssetCreateChComponent ,
            AssetCreateReComponent,
            AssetCreateOtComponent,
          BeneficiaryMainCreateComponent,
          BeneficiariesCreateComponent,
        DecreeListComponent,
          DecreeCardComponent,
            AssetDescriptionComponent,
            BeneficiaryMainItemComponent,
            BeneficiariesListComponent,
              BeneficiaryItemComponent
  ],
  imports: [
    BrowserModule,
    routing,
    HttpModule,
    ReactiveFormsModule,
    FormsModule
  ],
  bootstrap: [AppComponent],
  providers: [
    WillService,
    TestatorService,
    DecreeService
  ]
})
export class AppModule {

}
